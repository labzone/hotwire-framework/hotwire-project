<?php

require __DIR__.'/../vendor/autoload.php';

use HotWire\Framework\App\AbstractKernel;

class AppKernel extends AbstractKernel
{
    public function getApps()
    {
        return array(
            new Acme\Demo\App(),
            new HotWire\Form\App(),
            new HotWire\ORM\App(),
            new HotWire\Framework\App(),
            new HotWire\Mailer\App(),
            new HotWire\RedPlates\App(),
            new HotWire\EventDispatcher\App()
        );
    }
}
