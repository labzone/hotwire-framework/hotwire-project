<?php

namespace Acme\Demo\Subscriber;

use HotWire\EventDispatcher\ISubscriber;
use Acme\Demo\Event\ArticleEvent;

class ArticleSubscriber implements ISubscriber
{
    public function execute(ArticleEvent $event)
    {
        $event->getData()->setCreatedOn(new \DateTime());
    }

    public function getSubscribedEvents()
    {
        return array(
            ArticleEvent::PREPERSIST => 'execute'
        );
    }
}
