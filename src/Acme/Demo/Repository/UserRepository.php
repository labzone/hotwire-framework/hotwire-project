<?php

namespace Acme\Demo\Repository;

use HotWire\ORM\AbstractRepository;

class UserRepository extends AbstractRepository
{
    public function getEntityType()
    {
        return 'Acme:Demo::User';
    }
}
