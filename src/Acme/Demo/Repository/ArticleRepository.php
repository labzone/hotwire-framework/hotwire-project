<?php

namespace Acme\Demo\Repository;

use HotWire\ORM\AbstractRepository;

class ArticleRepository extends AbstractRepository
{
    public function getEntityType()
    {
        return 'Acme:Demo::Article';
    }
}
