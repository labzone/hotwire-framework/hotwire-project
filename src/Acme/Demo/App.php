<?php

namespace Acme\Demo;

use HotWire\Framework\AbstractApp;

class App extends AbstractApp
{
    public function getExtension()
    {
        return null;
    }

    public function getName()
    {
        return 'Acme:Demo';
    }
}
