<?php

namespace Acme\Demo\Form;

use HotWire\Form\Builder\IBuilder;
use HotWire\Form\AbstractForm;

class ArticleForm extends AbstractForm
{
    public function build(IBuilder $builder)
    {
        $builder->add('title')
                ->add('introduction')
                ->add('text','textarea')
                ->add('submit','button');
    }
}
