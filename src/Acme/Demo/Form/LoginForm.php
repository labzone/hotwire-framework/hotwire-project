<?php

namespace Acme\Demo\Form;

use HotWire\Form\Builder\IBuilder;
use HotWire\Form\AbstractForm;

class LoginForm extends AbstractForm
{
    public function build(IBuilder $builder)
    {
        $builder->add('username')
                ->add('password');
    }
}
