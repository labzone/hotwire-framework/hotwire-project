<?php

namespace Acme\Demo\Entity;

use HotWire\ORM\IEntity;

/**
 * @use HotWire\ORM\Schema
 *
 **/
class User implements IEntity
{
    /**
     * @Id
     */
    private $id;

    /**
     * @String(100)
     */
    private $username;

    /**
     * @String(255)
     */
    private $password;

    /**
     * @String(110,null)
     **/
    private $firstName;

    /**
     * @String(110,null)
     **/
    private $lastName;

    /**
     * Gets the value of id.
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public function setUsername($username)
    {
        $this->username=$username;

        return $this;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setPassword($password)
    {
        $this->password=$password;

        return $this;
    }

    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Gets the value of firstName.
     *
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Sets the value of firstName.
     *
     * @param mixed $firstName the first name
     *
     * @return self
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Gets the value of lastName.
     *
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Sets the value of lastName.
     *
     * @param mixed $lastName the last name
     *
     * @return self
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }
}
