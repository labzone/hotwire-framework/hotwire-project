<?php

namespace Acme\Demo\Controller;

use HotWire\Http\Request;
use HotWire\Http\Response;

class DefaultController
{
    public function indexAction(Request $request, $name)
    {
        return new Response($name);
    }
}
