<?php

namespace Acme\Demo\Controller;

use HotWire\Http\Request;

use HotWire\Framework\Controller\Controller;

class HomeController extends Controller
{
    public function indexAction(Request $request, $name)
    {
        return $this->render('Acme:Demo::Home/index',[
            'name'=>$name
        ]);
    }
}
