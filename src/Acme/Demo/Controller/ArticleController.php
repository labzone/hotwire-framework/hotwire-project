<?php

namespace Acme\Demo\Controller;

use Acme\Demo\Entity\Article;
use Acme\Demo\Entity\User;
use Acme\Demo\Form\ArticleForm;
use Acme\Demo\Event\ArticleEvent;
use Acme\Demo\Subscriber\ArticleSubscriber;

use HotWire\Framework\Controller\Controller;
use HotWire\Http\Request;
use HotWire\ORM\Manager;
use HotWire\ORM\DataMapper;
use HotWire\EventDispatcher\Listener;

class ArticleController extends Controller
{

    private $manager;
    private $listener;

    public function __construct(Manager $manager, DataMapper $mapper, Listener $listener)
    {
        $this->manager=$manager;
        $mapper->setEntity(new User())
               ->map()->create();
        $mapper->setEntity(new Article())
               ->map()->create();
        $listener->add(new ArticleSubscriber());
        $this->listener=$listener;
    }

    public function indexAction()
    {
        $articles=$this->manager
                       ->getRepository('Acme:Demo::ArticleRepository')
                       ->findAll();

        return $this->render('Acme:Demo::Article/index',[
            'articles'=>$articles
        ]);
    }

    public function showAction($id)
    {
        $article=$this->manager
                      ->getRepository('Acme:Demo::ArticleRepository')
                      ->find($id);

        $this->manager->persist($article);

        return $this->render('Acme:Demo::Article/show',[
            'article'=>$article
        ]);
    }

    public function createAction(Request $request)
    {
        $form=new ArticleForm();
        if ($request->isPost()) {
            $article=$form->handle($request, new Article());
            $event=new ArticleEvent();
            $event->setData($article);
            $this->listener->dispatch(ArticleEvent::PREPERSIST,$event);
            $this->manager->persist($event->getData());

            return $this->redirect('/');
        }

        return $this->render('Acme:Demo::Article/create',[
            'form'=>$form->render()
        ]);
    }

    public function removeAction(Request $request,$id)
    {
         if($article=$this->manager
                       ->getRepository('Acme:Demo::ArticleRepository')
                       ->find($id)){
            $this->manager->remove($article);
        }

        return $this->redirect('/');
    }
}
