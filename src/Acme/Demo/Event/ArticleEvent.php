<?php

namespace Acme\Demo\Event;

use HotWire\EventDispatcher\IEvent;

class ArticleEvent implements IEvent
{
    const PREPERSIST='article.prepersist';

    private $article;

    public function getData()
    {
        return $this->article;
    }

    public function setData($data)
    {
        $this->article=$data;

        return $this;
    }
}
